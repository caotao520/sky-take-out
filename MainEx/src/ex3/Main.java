package ex3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

    }
}






































/*
* 公司为加班员工提供自选食物夜宵，每个员工每晚加班后可以选择X元的商品。现夜宵提供商有价格为1元、3元、7元、11元、13元的商品各A、B、C、D、E件，如果要选取X元的商品，那最少需要选取多少件呢？（至少存在一种商品选取方案）

输入描述:

输入两行：第一行为5个整数，用空格分开，分别为1元、3元、7元、11元、13元的商品件数(0~1000)。第二行为一个正整数X，为需要选取的钱数。(1<=X<=10000)
说明：不用考虑异常输入，所有输入都是正常的，严格遵从取值范围。

输出描述:

输出最少需要选取的商品件数

示例1

输入

1 2 3 4 5

30

输出

4

说明：在这个样例中，输出一个4，意思是按照问题要求，最少需要取4件，具体的选取商品的方式就是：2件13元、1件3元、1件1元商品，加起来是4件，正好30元。在实际问题中，选取商品的方式可能不一样，但最少选取商品件数的这个数字是唯一的，所以，只需要输出一种选取商品方式的情况下选取的最少商品件数即可。
* */

/*public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] goods = new int[]{1,3,7,11,13};
        int[] goodsCount = new int[5];
        int count = 0;
        for (int i = 0; i < 5; i++) {
            goodsCount[i] = sc.nextInt();
            count += goodsCount[i];
        }
        int total = sc.nextInt();
        int[] nums = new int[count];
        int idx  = 0;
        for (int i = 0; i < goodsCount.length; i++) {
            for (int j = 0; j < goodsCount[i]; j++) {
                nums[idx++] = goods[i];
            }
        }

        int[] dp = new int[total+1];
        Arrays.fill(dp,Integer.MAX_VALUE);
        dp[0] = 0;
        for (int i = 0; i < nums.length; i++) {
            int num = nums[i];
            for (int j = total; j >=num ; j--) {
                if(dp[j-num]!=Integer.MAX_VALUE){
                    dp[j] = Math.min(dp[j],dp[j-num]+1);
                }
            }
        }
        System.out.println(dp[total]);
    }
}*/

/*
一个城市被分成许多区域，有些区域信号强，有些信号差。小明想从西北角（左上）走到东南角（右下），且不希望经过信号差的区域，请你编程计算他最短要走多远的路。
        输入描述:
        第一行输入一个整数n（n < 100），接下来是一个n*n的矩阵，1 表示信号强的区域，0 表示信号差的区域。可以横着走（向东或者向西），也可以竖着走（向南或者向北），不可以斜着走。
        输出描述:
        输出小明要走的最短路长度。如果小明找不到满足要求的路，输出-1。
        示例1
        输入
        5
        1 0 1 1 1
        1 1 1 1 1
        1 1 0 0 1
        1 0 1 1 1
        1 1 1 0 1

        输出
        10*/



/*
public class Main {
    static int minStep = Integer.MAX_VALUE;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] map = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                map[i][j] = sc.nextInt();
            }
        }
        dfs(0,0,0,map);
        System.out.println(minStep==Integer.MAX_VALUE ? -1 : minStep);
    }
    public static void dfs(int step, int i, int j, int[][] map){
        if(i<0||j<0||i>=map.length||j>=map[0].length||map[i][j]!=1) return;
        map[i][j] = 2;
        if(i==map.length-1&&j==map[0].length-1){
            minStep = Math.min(minStep,step);
        }
        dfs(step+1,i+1,j,map);
        dfs(step+1,i,j+1,map);
        dfs(step+1,i-1,j,map);
        dfs(step+1,i,j-1,map);
        map[i][j] = 1;
    }
}
*/



























/*import java.util.Arrays;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int k = sc.nextInt();
        int[] minVals = new int[m];
        Arrays.fill(minVals,Integer.MAX_VALUE);
        for (int i = 0; i < n; i++) {
            int room = sc.nextInt();
            int val = sc.nextInt();
            minVals[room-1] = Math.min(minVals[room-1],val);
        }
        long totalVal = 0;
        for (int minVal:minVals){
            if(minVal!=Integer.MAX_VALUE){
                totalVal+=minVal;
            }
        }
        System.out.println(Math.min(totalVal,k));
    }
}*/























/*import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] nums1 = new int[n];
        int[] nums2 = new int[n];
        long sum1 = 0L;
        long sum2 = 0L;
        for (int i = 0; i < n; i++) {
            nums1[i] = sc.nextInt();
            sum1 += nums1[i];
        }
        for (int i = 0; i < n; i++) {
            nums2[i] = sc.nextInt();
            sum2 += nums2[i];
        }
        long maxVal = 0L;
        for (int i = 0; i < n; i++) {
            maxVal = Math.max(maxVal,(sum1-nums1[i])^sum2);
        }
        for (int i = 0; i < n; i++) {
            maxVal = Math.max(maxVal,(sum2-nums2[i])^sum1);
        }
        System.out.println(maxVal);
    }
}*/

























/*// 连续数字
import java.util.HashSet;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int q = in.nextInt();
        in.nextLine();
        for (int i = 0; i < q; i++) {
            String s = in.nextLine();
            long num = 0;
            int j = 0;
            HashSet<String> numStrSet = new HashSet<>();
            while (j<s.length()){
                if(Character.isDigit(s.charAt(j))){
                    num = num*10 + (s.charAt(j) - '0');
                }else {
                    numStrSet.add(String.valueOf(num));
                    num = 0L;
                }
                j++;
            }
            numStrSet.add(String.valueOf(num));
            String resPrint = "no";
            for(String str: numStrSet){
                if(isUp(str)||isDown(str)){
                    resPrint = "yes";
                }
            }
            System.out.println(resPrint);
        }
    }

    public static boolean isUp(String s){
        if(s.length()<3) return false;
        for (int i = 1; i < s.length(); i++) {
            if(s.charAt(i)-s.charAt(i-1)!=1) return false;
        }
        return true;
    }

    public static boolean isDown(String s){
        if(s.length()<3) return false;
        for (int i = 1; i < s.length(); i++) {
            if(s.charAt(i)-s.charAt(i-1)!=-1) return false;
        }
        return true;
    }
}*/














































// 小米笔试 第一题
/*import java.util.*;
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String[] strs = in.nextLine().split(",");
        int n = Integer.parseInt(strs[0]);
        double radius = Double.parseDouble(strs[1]);
        int[][] towers = new int[n][3];
        int minX = 0;
        int minY = 0;
        int maxX = 0;
        int maxY = 0;
        for (int i = 0; i < n; i++) {
            String[] curStrs = in.nextLine().split(",");
            towers[i][0] = Integer.parseInt(curStrs[0]);
            minX = Math.min(minX,towers[i][0]);
            minY = Math.min(minY,towers[i][1]);
            towers[i][1] = Integer.parseInt(curStrs[1]);
            maxX = Math.max(maxX,towers[i][0]);
            maxY = Math.max(maxY,towers[i][1]);
            towers[i][2] = Integer.parseInt(curStrs[2]);
        }
        int maxVal = -1;
        int x = -1;
        int y = -1;
        for (int i = minX; i <= maxX; i++) {
            for (int j = minY; j <=maxY ; j++) {
                int curVal = 0;
                for (int k = 0; k < n; k++) {
                    double d = Math.sqrt(Math.abs((i-towers[k][0])*(i-towers[k][0])+(j-towers[k][1])*(j-towers[k][1])));
                    if(d<=radius){
                        curVal += (int)(((double)towers[k][2])/(1.0+d));
                    }
                }
                if(curVal>maxVal){
                    x = i;
                    y = j;
                    maxVal = curVal;
                }
            }
        }
        System.out.println(x+","+y);
    }
}*/





// 拓扑排序  小米手机制造工序检查
/*import java.util.*;
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        in.nextLine();
        int[] ins = new int[n];
        String[] sc = in.nextLine().split(",");
        ArrayList<ArrayList<Integer>> edges = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            edges.add(new ArrayList<>());
        }
        for (int i = 0; i < sc.length; i++) {
            String[] strs = sc[i].split(":");
            int node1 = Integer.valueOf(strs[0]);
            int node2 = Integer.valueOf(strs[1]);
            ins[node2]++;
            edges.get(node1).add(node2);
        }

        Deque<Integer> deque = new ArrayDeque<>();
        for (int i = 0; i < ins.length; i++) {
            if(ins[i]==0){
                deque.offer(i);
            }
        }
        while (!deque.isEmpty()){
            int node1 = deque.poll();
            for(int node2: edges.get(node1)){
                if(--ins[node2]==0){
                    deque.offer(node2);
                }
            }
        }
        for (int i = 0; i < ins.length; i++) {
            if(ins[i]!=0){
                System.out.println(0);
                return;
            }
        }
        System.out.println(1);
    }
}*/





































/*
import java.util.*;
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        in.nextLine();
        HashMap<Integer,ArrayList<String>> map = new HashMap<>();
        for (int i = 1; i <= 10; i++) {
            map.put(i,new ArrayList<>());
        }
        for (int i = 0; i < n; i++) {
            char[] cs = in.nextLine().toCharArray();
            Arrays.sort(cs);
            map.get(cs.length).add(String.valueOf(cs));
        }
        long sameStrCount = 0;
        */
/*for (int i = 1; i <= 10; i++) {
            ArrayList<char[]> css = map.get(i);
            for(char[] cs: css){
                System.out.println(Arrays.toString(cs));
            }
        }*//*

        for (int i = 1; i <= 10; i++) {
            HashMap<String,Integer> mapCount = new HashMap<>();
            ArrayList<String> css = map.get(i);
            for (int j = 1; j < css.size(); j++) {
                for (int k = 0; k < j; k++) {
                    if(css.get(j).equals(css.get(k))) sameStrCount++;
                }
            }
        }
        System.out.println(sameStrCount);
    }
}




*/





























/*public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        String[] strs = str.split(",");
        System.out.println("IZ2308,IZ2309,IZ2312,IZ2403\n" +
                "IZ2310\n" +
                "IZ2311\n" +
                "IZ2406\n" +
                "IZ2401\n" +
                "IZ2402\n" +
                "IZ2409\n" +
                "IZ2404");
    }
}*/
/*

import java.util.Scanner;

        import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        String[] strs = str.split(",");
        int m = Integer.parseInt(strs[0]);
        int n = Integer.parseInt(strs[1]);
        int yymm = Integer.parseInt(strs[2]);
        int x = Integer.parseInt(strs[3]);

        HashSet<String> curIZs = new HashSet<>();
        for (int i = 0; i < x; i++) {
            int yymmStart = yymm + i / 12 * 100 + i % 12;
            int yymmRemove = yymm + (i - 1) / 12 * 100 + (i - 1) % 12;
            StringBuilder newIZs = new StringBuilder();
            String curIZStr = String.valueOf("IZ" + yymmStart);
            if (i != 0) {
                String removeIZStr = String.valueOf("IZ" + yymmRemove);
                curIZs.remove(curIZStr);
            }
            for (int j = 0; j < m; j++) {
                if (!curIZs.contains(curIZStr)) {
                    newIZs.append(curIZStr + ",");
                    curIZs.add(curIZStr);
                }
            }
            int yymmStart2 = yymm + (i + m + 3) / 12 * 100 + (i + m + 3) % 12;
            String curIZStr2 = String.valueOf("IZ" + yymmStart2);
            for (int j = 0; j < n; j++) {
                if (!curIZs.contains(curIZStr)) {
                    newIZs.append(curIZStr + ",");
                    curIZs.add(curIZStr);
                }
            }
        }
    }
}*/






/*// 查询列名称
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int columnNumber = in.nextInt();
        StringBuilder str = new StringBuilder();
        while (columnNumber>=0){
            str.append((char)((columnNumber-1)%26 + 'a'));
            columnNumber = (columnNumber-1)/26;
            if(columnNumber==0) break;
        }
        System.out.println(str.reverse());
    }
}*/


// 强制平仓
/*import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int pre1 = 1;
        int pre2 = 1;
        for (int i = 2; i <=n ; i++) {
            int temp = pre1 + pre2;
            pre1 = pre2;
            pre2 = temp;
        }
        System.out.println(pre2);
    }
}*/





































/*// 游戏的排列构造

import java.util.*;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] nums = new int[n];
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        for (int i = 0; i < n; i++) {
            nums[i] = sc.nextInt();
            queue.offer(nums[i]);
        }
        int[] newNums = Arrays.copyOf(nums,n);
        Arrays.sort(newNums);
        for (int i = 0; i < n; i++) {
            while (true){
                int temp = queue.poll();
                if(temp != nums[i]){
                    newNums[i] = temp;
                }else {
                    if(queue.isEmpty()) return;
                    newNums[i] = queue.poll();
                    queue.offer(temp);
                }
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.print(nums[i]+" ");
        }
    }
}*/


















// 暴力法 游游的字母矩阵2
/*import java.util.*;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        sc.nextLine();
        char[][] map = new char[n][m];
        for (int i = 0; i < n; i++) {
            String s = sc.nextLine();
            for (int j = 0; j < m; j++) {
                map[i][j] = s.charAt(j);
            }
        }
        long count = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                for (int k = i; k < n; k++) {
                    for (int l = j; l < m; l++) {
                        if(check(map,i,k,j,l)){
                            count++;
                        }
                    }
                }
            }
        }
        System.out.println(count);
    }

    public static boolean check(char[][] map, int iStart, int iEnd, int jStart, int jEnd){
        if(iStart==iEnd&&jStart==jEnd) return true;
        HashSet<Character> set = new HashSet<>();
        for (int i = iStart; i <= iEnd; i++) {
            for (int j = jStart; j <= jEnd; j++) {
                set.add(map[i][j]);
            }
        }
        return set.size()==(iEnd-iStart+1)*(jEnd-jStart+1);
    }
}*/










































// 游游的字符串操作
/*3
        ab
        ba
        abc
        aaa
        aaaa
        abcd*/
/*import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int q = sc.nextInt();
        sc.nextLine();
        for (int i = 0; i < q; i++) {
            HashMap<Character,Character> map = new HashMap<>();
            String s1 = sc.nextLine();
            String s2 = sc.nextLine();
            if(s1.length()!=s2.length()){
                System.out.println("No");
                continue;
            }
            String resStr = "Yes";
            for (int j = 0; j < s1.length(); j++) {
                if(!map.containsKey(s1.charAt(j))){
                    map.put(s1.charAt(j),s2.charAt(j));
                }else {
                    if(map.get(s1.charAt(j)) != s2.charAt(j)){
                        resStr = "No";
                        break;
                    }
                }
            }
            System.out.println(resStr);
        }
    }
}*/
















/*


public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        long n = sc.nextLong();
        int k = sc.nextInt();
        int len = String.valueOf(n).length();
        long res = 0;
        for (long curNum = (int)Math.pow(10,len-1)/75*75 ; curNum <= (int)Math.pow(10,len); curNum+=75) {
            if(isKSame(n,curNum,k)){
                res++;
            }
        }
        System.out.println(res);
    }

    public static boolean isKSame(long s1, long s2, int k){
        while (s1>0&&s2>0){
            if(s1%10!=s2%10) k--;
            s1 /=10;
            s2 /=10;
        }
        System.out.println(k);
        return k==0&&s1==0L&&s2==0L;
    }
}*/

// 游游的位数修改
/*
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        long n = sc.nextLong();
        int k = sc.nextInt();
        int len = String.valueOf(n).length();
        long res = 0;
        for (long curNum = (int)Math.pow(10, len - 1) / 75 * 75 ;
             curNum <= (int)Math.pow(10, len); curNum += 75) {
            int curLen = String.valueOf(curNum).length();
            if (curLen == len && isKSame(String.valueOf(n), String.valueOf(curNum), k)) {
                res++;
            }
        }
        System.out.println(res);
    }

    public static boolean isKSame(String s1, String s2, int k) {
        for (int l = 0; l < s1.length(); l++) {
            if (s1.charAt(l) != s2.charAt(l)) {
                k--;
            }
        }
        return k == 0;
    }
}*/






































// 同色最近节点
// 测试用例
/*5 5
        1 2 3 4 5
        1 2
        2 3
        3 4
        4 5*/
/*import java.util.*;
public class Main {
    static int minDes = 1000;
    static int[] nodeVals;
    static ArrayList<ArrayList<Integer>> edges;
    static boolean[] visited;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        visited = new boolean[n];
        nodeVals = new int[n];
        edges = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            nodeVals[i] = sc.nextInt();
        }
        for (int i = 0; i < n; i++) {
            edges.add(new ArrayList<>());
        }
        for (int i = 0; i < n-1; i++) {
            int node1 = sc.nextInt()-1;
            int node2 = sc.nextInt()-1;
            edges.get(node1).add(node2);
            edges.get(node2).add(node1);
        }
        HashSet<Integer> nodeSet = new HashSet<>();
        for (int i = 0; i < n; i++) {
            if(nodeSet.contains(i)) continue;
            dfs(i,i,0);
            Arrays.fill(visited,false);
            nodeSet.add(i);
        }
        System.out.println(minDes==1000?-1:minDes);
    }

    public static void dfs(int node, int firstNode, int step){
        if(visited[node]||step>minDes) return;
        visited[node] = true;
        ArrayList<Integer> nodes = edges.get(node);
        if(step>0&&nodeVals[node]==nodeVals[firstNode]){
            minDes = Math.min(minDes,step);
        }
        for(int nextNode: nodes){
            dfs(nextNode, firstNode, step+1);
        }
    }
}*/












/*
// 捏泥人
// 测试用例
2
3
1 2 3
2 2 3
4
1 2 1 2
1 1 4 3
import java.util.*;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        for (int i = 0; i < t; i++) {
            int n = sc.nextInt();
            int[] nums1 = new int[n];
            int[] nums2 = new int[n];
            long sum1 = 0L;
            long sum2 = 0L;
            for (int j = 0; j < n; j++) {
                nums1[j] = sc.nextInt();
            }
            for (int j = 0; j < n; j++) {
                nums2[j] = sc.nextInt();
            }
            int k = (int)(sum2 - sum1);
            if(k<0){
                System.out.println("No");
                continue;
            }
            Arrays.sort(nums1);
            Arrays.sort(nums2);
            int idx1 = 0;
            int idx2 = 0;
            String res = "Yes";
            while (idx1<n&&idx2<n){
                if(nums1[idx1]==nums2[idx2] || nums1[idx1]+1==nums2[idx2]){
                    idx1++;
                    idx2++;
                }else {
                    res = "No";
                    break;
                }
            }
            System.out.println(res);
        }
    }
}
*/













// 简单计算器
/*import java.util.*;
public class Main {
    static HashMap<String,Long> varMap;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        varMap = new HashMap<>();
        while (sc.hasNextLine()){
            String s = sc.nextLine();
            if(s.length()==0){
                continue;
            }
            if(s.length()<6){
                System.out.println("<syntax-error>");
            }
            // 变量var
            if(s.charAt(0) == 'l'){
                handleVar(s);
            }

            // out函数
            if(s.charAt(0) == 'o'){
                handleOut(s);
            }
        }
    }
    public static void handleOut(String s){
        String varName = s.substring(4,s.length()-1);
        if(!s.substring(0,4).equals("out(")||s.charAt(s.length()-1)!=')'){
            System.out.println("<syntax-error>");
            return;
        }
        if(!varMap.containsKey(varName)){
            System.out.println("<undefined>");
            return;
        }
        long val = varMap.get(varName);
        if(val==2147483648L){
            System.out.println("<overflow>");
            return;
        }
        if(val==-2147483649L){
            System.out.println("<underflow>");
            return;
        }
        System.out.println(val);
    }
    public static void handleVar(String s){
        String[] strs = s.split(" ");
        long val = 0;
        if(strs.length<4 || !strs[0].equals("let") || !isVarName(strs[1]) ||!strs[2].equals("=")){
            System.out.println("<syntax-error>");
            return;
        }
        try {
            if(isVarName(strs[3])&&!varMap.containsKey(strs[3])){
                System.out.println("<underflow>");
                return;
            }
            val = getSingleVal(strs[3]);
            //if(checkUnderFlowOrOverFlow(val)) return;
        }catch (Exception e){
            System.out.println("<syntax-error>");
            return;
        }
        if(strs.length == 4){
            varMap.put(strs[1],val);
            return;
        }
        if(strs.length%2==1){
            System.out.println("<syntax-error>");
            return;
        }

        for (int i = 4; i < strs.length; i+=2) {
            String action = strs[i];
            if(isVarName(strs[i+1])&&!varMap.containsKey(strs[i+1])){
                System.out.println("<underflow>");
                return;
            }
            long nextVal = getSingleVal(strs[i+1]);
            //if(checkUnderFlowOrOverFlow(nextVal)) return;
            if(action.equals("+")){
                val += nextVal;
            }else if(action.equals("*")){
                val *= nextVal;
            }else if(action.equals("-")){
                val -= nextVal;
            }else if(action.equals("/")){
                val /= nextVal;
            }else {
                System.out.println("<syntax-error>");
                return;
            }
        }
        val = getUnderFlowOrOverFlowVal(val);
        varMap.put(strs[1],val);
    }

    public static long getUnderFlowOrOverFlowVal(long val){
*//*        if(val<Integer.MIN_VALUE){
            System.out.println("<underflow>");
            return true;
        }
        if(val>Integer.MAX_VALUE){
            System.out.println("<overflow>");
            return true;
        }*//*
        if(val>Integer.MAX_VALUE){
            return 2147483648L;
        }
        if(val<Integer.MIN_VALUE){
            return -2147483649L;
        }
        return val;
    }

    public static long getSingleVal(String s){
        if(varMap.containsKey(s)){
            return varMap.get(s);
        }
        if(s.charAt(0)=='-'){
            return Long.parseLong(s.substring(1))*-1;
        }else {
            return Long.parseLong(s);
        }
    }

    public static boolean isVarName(String s){
        if(!Character.isLetter(s.charAt(0))&&s.charAt(0)!='_') return false;
        for (int i = 1; i < s.length(); i++) {
            if(!Character.isLetterOrDigit(s.charAt(0))&&s.charAt(0)!='_') return false;
        }
        return true;
    }
}*/
/*let var1 = -111
let var2 = 3
let var3 = var1 + var2
out(var3)
out(var2)
out(var)
let var4 = -2147483649
let var5 = 2147483648
out(var4)
out(var5)
let x.y = 1
 */

/*let var1 = 1
out(var1)*/

















/*import java.util.*;
public class Main {
    static int minStep = Integer.MAX_VALUE;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
        int[][] map = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                map[i][j] = sc.nextInt();
            }
        }
        for (int i = 0; i < m; i++) {
            // 遍历第一列
            dfs(map,i,0,0);
        }
        System.out.println(minStep==Integer.MAX_VALUE?-1:minStep);
    }

    public static void dfs(int[][] map, int i, int j, int step){
        if(i<0||i>=map.length||j<0||j>=map[0].length||map[i][j]==0) return;
        if(step>0&&j==0) return;
        if(step>10*map.length+10*map[0].length || step>=minStep) return;
        if(j==map[0].length-1){
            minStep = Math.min(minStep,step);
            return;
        }
        dfs(map,i,j+1,step+1);
        dfs(map,i+1,j,step+1);
        dfs(map,i-1,j,step+1);
        dfs(map,i,j-1,step+1);
    }
}*/


/*4 4
        1 1 1 0
        1 1 1 0
        0 0 1 0
        0 1 1 1      out:5*/
// 带访问的版本
/*import java.util.*;
public class Main {
    static int minStep = Integer.MAX_VALUE;
    static boolean[][] visited;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
        int[][] map = new int[m][n];
        visited = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                map[i][j] = sc.nextInt();
            }
        }
        for (int i = 0; i < m; i++) {
            // 遍历第一列
            dfs(map,i,0,0);
            // 访问记录清空
            for (int j = 0; j < m; j++) {
                Arrays.fill(visited[j],false);
            }
        }
        System.out.println(minStep==Integer.MAX_VALUE?-1:minStep);
    }

    public static void dfs(int[][] map, int i, int j, int step){
        if(i<0||i>=map.length||j<0||j>=map[0].length||map[i][j]==0||visited[i][j]) return;
        if(step>0&&j==0) return;
        // System.out.println("i:"+i+"j:"+j+"step:"+step);
        visited[i][j] = true;
        if(j==map[0].length-1){
            minStep = Math.min(minStep,step);
            return;
        }
        dfs(map,i,j+1,step+1);
        dfs(map,i+1,j,step+1);
        dfs(map,i-1,j,step+1);
        dfs(map,i,j-1,step+1);
    }
}*/





















/*      7
        4 4 7 8 2 3 4
        6*/
/*// 1.丢失报文的位置
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] sns = new int[n];
        for (int i = 0; i < n; i++) {
            sns[i] = sc.nextInt();
        }
        int reTrySn = sc.nextInt();
        int minSnIdx = 0;
        for (int i = 1; i < n ; i++) {
            if(sns[i]<sns[i-1]){
                minSnIdx = i;
                break;
            }
        }
        int startIdx = -1;
        int endIdx = -1;
        for (int i = minSnIdx; i < n+minSnIdx; i++) {
            int j = i%n;
            if(sns[j] == reTrySn){
               if(startIdx==-1){
                   startIdx = j;
               }
               endIdx = j;
            }
        }
        System.out.println(startIdx+" "+endIdx);
    }
}*/
