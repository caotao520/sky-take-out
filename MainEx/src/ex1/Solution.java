package ex1;

import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;
public class Solution{
    static ReentrantLock lock;
    static volatile int n = 1;
    public static void main(String[] args){
        lock = new ReentrantLock();
        Condition c1 = lock.newCondition();
        Condition c2 = lock.newCondition();
        Thread A = new Solution.ThreadN(c1,c2,0,2);
        Thread B = new ThreadN(c2,c1,1,2);
        A.start();
        B.start();

    }

    static class ThreadN extends Thread{
        private Condition c1;
        private Condition c2;
        private int x;
        private int step;
        ThreadN(Condition c1, Condition c2, int x, int step){
            this.c1 = c1;
            this.c2 = c2;
            this.x = x;
            this.step = step;
        }

        @Override
        public void run(){
            while(n<=99){
                lock.lock();
                try {
                    while(n%step!=x){
                        c1.await();
                    };
                    System.out.println(n);
                    n++;
                    c2.signal();
                } catch (Exception e) {
                    e.printStackTrace();

                }finally {
                    lock.unlock();
                }

            }
        }
    }
}
