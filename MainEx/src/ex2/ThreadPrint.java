package ex2;

class TreadPrint{
    static Object lock = new Object();
    static int state = 0;
    public static void main(String[] args) {
        TreadPrint treadPrint = new TreadPrint();
        new Thread(() ->{
            treadPrint.method('C',2);
        },"线程C").start();
        new Thread(() ->{
            treadPrint.method('B',1);
        },"线程B").start();
        new Thread(() ->{//使用这种传方法进去，注意把方法所在对象new出来再用对象.方法传进去
            treadPrint.method('A',0);
        },"线程A").start();
    }

    public void method(char letter,int targetState){
        for (int i = 0; i < 10; i++) {
            synchronized (lock){
                while (state%3!=targetState){
                    try {
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(Thread.currentThread().getName()+":"+letter);
                state++;
                lock.notifyAll();
            }
        }
    }
}
